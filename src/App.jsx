import './App.scss';
import { useState } from 'react';
import Axios from 'axios';

function App() {
    const [pokemonName, setPokemonName] = useState('');
    const [pokemonChosen, setPokemonChosen] = useState(false);
    const [pokemon, setPokemon] = useState({
        name: '',
        number: '',
        species: '',
        image: '',
        hp: '',
        attack: '',
        defense: '',
        speed: '',
        type: '',
    });
    const searchPokemon = () => {
        Axios.get(`https://pokeapi.co/api/v2/pokemon/${pokemonName}`).then((response) => {
            setPokemon({
                name: pokemonName,
                number: response.data.id,
                species: response.data.species.name,
                image: response.data.sprites.front_default,
                hp: response.data.stats[0].base_stat,
                attack: response.data.stats[1].base_stat,
                defense: response.data.stats[2].base_stat,
                speed: response.data.stats[5].base_stat,
                type: response.data.types[0].type.name,
            });
            setPokemonChosen(true);
        });
    };

    return (
        <div className="app">
            <div>
                <h1>Pokedex App!</h1>
                <input
                    type="text"
                    onChange={(event) => {
                        setPokemonName(event.target.value);
                    }}
                    value={pokemonName.toLowerCase()}
                />
                <button onClick={searchPokemon}>Search</button>
            </div>
            <div>
                {!pokemonChosen ? (
                    <h1>Please choose a Pokemon</h1>
                ) : (
                    <div>
                        <h1>{pokemon.name}</h1>
                        <img src={pokemon.image} alt={pokemon.name} />
                        <h3>Number: {pokemon.number}</h3>
                        <h3>Species: {pokemon.species}</h3>
                        <h3>Type: {pokemon.type}</h3>
                        <h4>HP: {pokemon.hp}</h4>
                        <h4>Attack: {pokemon.attack}</h4>
                        <h4>Defense: {pokemon.defense}</h4>
                        <h4>Speed: {pokemon.speed}</h4>
                    </div>
                )}
            </div>
        </div>
    );
}

export default App;
